package com.assignment2.entities;

import java.util.*;

import com.assignment2.entities.Producer;

public class Producer implements Runnable {

	private final Vector<Integer> sharedQueue;
	private final int SIZE;
	public boolean flag = true;

	public Producer(Vector<Integer> sharedQueue, int size) {
		this.sharedQueue = sharedQueue;
		this.SIZE = size;
	}

	@Override
	public void run() {
		int item = 0;

		try {
			while (flag == true) {
				produce(item);
				Thread.sleep(1000);
				item++;
			}
		} catch (InterruptedException ex) {
			System.out.println(Producer.class.getName() + " class exception : "
					+ ex);
		}
		finally{
			System.out.println("Producer Terminated!!!!!");
			synchronized (sharedQueue) {sharedQueue.notify();}
		}

	}

	public void stopProducer() {
		flag = false;
		

	}

	private void produce(int item) throws InterruptedException {
		if (sharedQueue.size() == SIZE) {
			synchronized (sharedQueue) {
				System.out.println("Queue is full "
						+ Thread.currentThread().getName()
						+ " is waiting , size: " + sharedQueue.size());
				sharedQueue.wait();
			}
		} else {
			synchronized (sharedQueue) {
				System.out.println("Produced: " + item);
				sharedQueue.add(item);
				sharedQueue.notifyAll();
			}
		}
	}
}
