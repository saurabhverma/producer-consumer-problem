package com.assignment2.entities;

import java.util.*;

import com.assignment2.entities.Consumer;

public class Consumer implements Runnable {

	private final Vector<Integer> sharedQueue;
	public boolean flag = true;


	public Consumer(Vector<Integer> sharedQueue) {
		this.sharedQueue = sharedQueue;
	}

	@Override
	public void run() {

		try {
			while (flag || sharedQueue.size() > 0) {
				
				consume();
				Thread.sleep(1500);
			}System.out.println("Consumer Terminated!!!!!");
		} catch (InterruptedException ex) {
			System.out.println(Consumer.class.getName() + " class exception : "
					+ ex);
		}
	}

	public void stopConsumer() {
		flag = false;
	}

	private void consume() throws InterruptedException {

		while (sharedQueue.isEmpty()) {
			synchronized (sharedQueue) {
				
				//Thread.sleep(1500);
				if(flag==false){
					System.out.println("Queue is empty & producer terminated.");
					break;
				}
				else{
					System.out.println("Queue is empty "
							+ Thread.currentThread().getName()
							+ " is waiting , size: " + sharedQueue.size());
				sharedQueue.wait();}
				
				}
				
			}
		synchronized (sharedQueue) {
			if(flag==false&&sharedQueue.isEmpty()){
				
			}else{
			sharedQueue.notifyAll();
			System.out.println("Consumed: " + sharedQueue.remove(0));
			}
		}
	}
}