package com.assignment2.controller;

import java.util.Scanner;
import java.util.Vector;

import com.assignment2.entities.*;

public class Controller {

	//protected static boolean flag;

	public static void main(String[] args) throws Exception {

		Scanner scannerObj = new Scanner(System.in);
		Vector<Integer> sharedQueue = new Vector<Integer>();
		int size = 20;
		Producer prod = new Producer(sharedQueue, size);
		Consumer cons = new Consumer(sharedQueue);
		Thread prodThread = new Thread(prod, "Producer");
		Thread consThread = new Thread(cons, "Consumer");
		prodThread.start();
		consThread.start();
		scannerObj.nextLine();
		prod.stopProducer();
		cons.stopConsumer();
	}
}
